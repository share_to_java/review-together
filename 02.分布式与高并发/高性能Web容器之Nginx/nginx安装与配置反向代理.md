# nginx安装与配置反向代理

> 操作系统：centos7
>
> nginx：



# 1、yum安装nginx

需要添加nginx的源

```shell
rpm -ivh http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm
```

安装

```shell
yum install -y nginx
```

> 以下是Nginx的默认路径：
>
> (1) Nginx配置路径：/etc/nginx/
>
> (2) PID目录：/var/run/nginx.pid
>
> (3) 错误日志：/var/log/nginx/error.log
>
> (4) 访问日志：/var/log/nginx/access.log
>
> (5) 默认站点目录：/usr/share/nginx/html

# 2、配置反向代理

* 注意，如果要配置80端口的反向代理，须将conf.d下面的default.conf的80端口删除或修改



在conf.d文件夹下创建mmdk.conf配置文件



### 1、强制https

```conf
server {
    listen       80;
    server_name  172.16.117.227;
    rewrite ^(.*)$ https://${server_name}$1 permanent;
}
```



### 2、https配置

openssl自签证书

```shell
#!/bin/sh

# create self-signed server certificate:

read -p "Enter your file name [example]: " FILENAME

read -p "Enter your domain [www.example.com]: " DOMAIN

echo "Create server key..."

openssl genrsa -des3 -out $FILENAME.key 1024

echo "Create server certificate signing request..."

SUBJECT="/C=US/ST=Mars/L=iTranswarp/O=iTranswarp/OU=iTranswarp/CN=$DOMAIN"

openssl req -new -subj $SUBJECT -key $FILENAME.key -out $FILENAME.csr

echo "Remove password..."

mv $FILENAME.key $FILENAME.origin.key
openssl rsa -in $FILENAME.origin.key -out $FILENAME.key

echo "Sign SSL certificate..."

openssl x509 -req -days 3650 -in $FILENAME.csr -signkey $FILENAME.key -out $FILENAME.crt

echo "TODO:"
echo "Copy $FILENAME.crt to /etc/nginx/ssl/$FILENAME.crt"
echo "Copy $FILENAME.key to /etc/nginx/ssl/$FILENAME.key"
echo "Add configuration in nginx:"
echo "server {"
echo "    ..."
echo "    listen 443 ssl;"
echo "    ssl_certificate     /etc/nginx/ssl/$FILENAME.crt;"
echo "    ssl_certificate_key /etc/nginx/ssl/$FILENAME.key;"
echo "}"
```



nginx配置

```conf

server {
    listen       443 default ssl;
    server_name  172.16.117.227;

    ssl_certificate /etc/nginx/ssl/nginx.crt;  # 指定证书的位置，绝对路径
    ssl_certificate_key /etc/nginx/ssl/nginx.key;  # 绝对路径，同上
    ssl_session_timeout 5m;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2; #按照这个协议配置
    ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:HIGH:!aNULL:!MD5:!RC4:!DHE;#按照这个套件配置
    ssl_prefer_server_ciphers on;
    location / {
		proxy_pass http://127.0.0.1:8080;
    }
}
```

