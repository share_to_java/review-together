# Nginx的初步认识及配置

课程目标 

1、Nginx在分布式架构中的应用分析 

2、常用的Web服务器及差异 

3、 Nginx的安装以及配置分析 

4、Nginx虚拟主机配置 

5、详解Location的匹配规则

---

## 什么是Nginx  

是一个高性能的反向代理服务器 

正向代理代理的是客户端 

反向代理代理的是服务端 

## Apache、Tomcat、Nginx

静态web服务器   nginx,apache原本是静态服务器，可以通过加装插件实现动态解析。

jsp/servlet服务器 tomcat

## 安装Nginx

1、下载tar包 

2、 tar -zxvf nginx.tar.gz 

3、./confifigure [--prefifix]   #--prefix=？ 指点安装路径

4、 make && make install 



## 启动和停止

1、sbin/nginx 

2、 ./nginx -s stop 



## nginx.conf

### Main

```shell

#user  nobody;
worker_processes  1;

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

#pid        logs/nginx.pid;
```



### event 

```shell
events {
    worker_connections  1024;
}

```



配置连接数等

### http

配置web服务器相关内容

### 虚拟主机配置

```shell
server {
	listen 80; server_name localhost; 
	#charset koi8-r; 
	#access_log logs/host.access.log main; 
	location / { 
		root html; 
		index index.html 
		index.htm; 
	} 
}
```

location：匹配规则。



### 基于ip的虚拟主机

不演示

### 基于端口号的虚拟主机

```shell
server {
	listen 8080; 
	server_name localhost; 
	location / { 
		root html; 
		index index.html; 
	} 
}
```

### 基于域名的虚拟主机

www.gupaoedu.com，bbs.gupaoedu.com，ask.gupaoedu.com

```shell
server {
	server_name www.gupaoedu.com; 
	location / { 
		root html; 
		index index.html; 
	} 
}
server { 
	listen 80; 
	server_name bbs.gupaoedu.com; 
	location / { 
		root html; 
		index bbs.html; 
	} 
}
server { 
	listen 80; 
	server_name ask.gupaoedu.com; 
	location / { 
		root html; 
		index ask.html; 
	} 
}
```

### location

 ### 配置语法

`location [= | ~* | ^~ ] /uri/ {...} `

### 配置规则

location = /uri 精准匹配 

location ^~ /uri 前缀匹配 

location ~ /uri 

location / 通用匹配

### 规则的优先级

```shell
1 location = / 
2 location = /index 
3 location ^~ /article/ 
4 location ^~ /article/files/ 
5 location ~ \.(gif|png|js|css)$ 
6 location / 
匹配实例：
http://192.168.11.154/ -> 1
http://192.168.11.154/index ->2 
http://192.168.11.154/article/files/1.txt ->4 
http://192.168.11.154/mic.png ->5
```

1. 精准匹配是优先级最高 

2. 普通匹配（最长的匹配） 

3. 正则匹配

### 实际使用建议

```shell
location =/ { 

}
location / { 

}

location ~* \.(gif|....)${ 

}
```

### Nginx模块

反向代理、email、nginx core。。。

### 模块分类

1. 核心模块 ngx_http_core_module 

2. 标准模块 http模块 

3. 第三方模块

### ngx_http_core_module

```shell
server{
listen port
server_name
1 location = / 
2 location = /index 
3 location ^~ /article/ 
4 location ^~ /article/files/ 
5 location ~ \.(gif|png|js|css)$ 
6 location / 
http://192.168.11.154/ 
http://192.168.11.154/index ->2 
http://192.168.11.154/article/files/1.txt ->4 
http://192.168.11.154/mic.png ->5 location =/ { }location / { }location ~* \.(gif|....)${ }
root ...
}
```

location 实现uri到文件系统路径的映射

2. error_page

### ngx_http_access_module

实现基于ip的访问控制功能 

1、allow address | CIDR | unix: | all; 

2、deny address | CIDR | unix: | all; 

自上而下检查，一旦匹配，将生效，条件严格的置前 

### 如何添加第三方模块

1. 原来所安装的配置，你必在重新安装新模块的时候，加上 
2. 不能直接make install

查看编译配置`./nginx -V`

加上之前的配置

```shell
configure --prefix=/data/program/nginx
```

### 安装方法

```shell
./configure --prefix=/安装目录 --add-module = /第三方模块的目录 
./configure --prefix=/data/program/nginx --with-http_stub_status_module --with- http_random_index_module 
make
cp objs/nginx $nginx_home/sbin/nginx
```

如果直接make &&  make install 则会覆盖之前的所有配置，所以用`cp`替换

### http_stub_status_module

```shell
location /status { stub_status; }
```

Active connections:当前状态，活动状态的连接数 

accepts：统计总值，已经接受的客户端请求的总数 

configure --prefix=/data/program/nginx 

./configure --prefix=/安装目录 --add-module = /第三方模块的目录 

./configure --prefix=/data/program/nginx --with-http_stub_status_module --with- 

http_random_index_module 

cp objs/nginx $nginx_home/sbin/nginx 

location /status { 

stub_status; 

}handled：统计总值，已经处理完成的客户端请求的总数 

requests：统计总值，客户端发来的总的请求数 

Reading：当前状态，正在读取客户端请求报文首部的连接的连接数 

Writing：当前状态，正在向客户端发送响应报文过程中的连接数 

Waiting：当前状态，正在等待客户端发出请求的空闲连接数 



### http_random_index_module

www.gupaoedu.com 

随机显示主页 

一般情况下,一个站点默认首页都是定义好的index.html、index.shtml等等,如果想站点下有很多页面想随机展示给 

用户浏览,那得程序上实现，很麻烦，使用nginx的random index即可简单实现这个功能，凡是以/结尾的请求，都 

会随机展示当前目录下的文件作为首页 

1. 添加random_index on 配置，默认是关闭的 

```shell
location / { 
	root html; 
	random_index on; 
	index index.html index.htm; 
}
```

2. 在html目录下创建多个html页面