# 一篇文章让你学会Maven

## 概述

	无利不起早是人的天性，我也一样o(*￣︶￣*)o。我一般写一些文字的时候，主要目的是梳理自己大脑里一些不清晰
	的思路，分享是其次，哈哈。网上也有很多Maven资料不过有的不全，有的又太复杂，本文希望用一篇文章就把Maven
	的主要知识点全部讲完。

## 认识Maven

	管理项目的依赖包，在以前没有项目管理工具的时候，项目依赖包的配置都是直接导入到build path来导入的。一个
	是自己管理起来很乱，二 是团队管理很麻烦。

## Maven的优势

* 约定优于配置

```
约定优于配置，Maven项目有一个超级pom.xml,超级pom中的配置就是约定。
```

* 简单
* 测试支持（支持单元测试）
* 构建简单
* CI
* 插件丰富



## 安装/配置Maven

### 下载	

	下载Maven压缩包。

### 安装

	解压压缩包。配置环境变量。
	
	   *maven用的是自己的类加载器*

* 在Maven的conf下的setting文件是Maven的全局配置。
* Maven的超级pom在lib下的maven-mode-builder.x.x.x.jar包中，解压可得。

## Maven配置

* 配置PATH环境变量：
  * Windows 配置path路径；
  * Linux 配置.bash_profile

* 配置MAVN_HOME 环境变量

* Maven JVM参数设置

  ```
  配置MAVEM_OPTS，好处：
  
  - 不用每次都把选项在命令行输入；
  - 统一配置，大家都一样，方便管理。
  ```

  * 

* 用户目录配置（setting.xml）

  ```
  maven 配置文件加载顺序： ~/.m2/setting.xml -> conf/setting/xml
  ```

  在setting.xml文件中配置用户仓库目录和镜像等。



* 部分配置项说明
  * pluginGroup :maven 插件管理
  * proxies:代理（翻墙）
  * server：在自己搭建maven私服的时候配置 用户名密码
  * mirror：镜像
  * profile：切换环境（开发环境、测试环境、生产环境）
  * 



## 新建Maven项目

	maven项目目录结构

`mvn dependency:tree > d.txt`

* pom.xml

  * groupId:

  * artifactId:功能名

  * version：版本

  * packaging:打包方式（默认jar包）

  * description:描述

  * properties：定义一些变量，可以在配置的其他部分使用。

  * dependencyManagement：

    * 只出现在父pom.xml里面（对自己的要求，并不是不能实现在子pom.xml使用）

    * 同一版本号

    * 申明：dependencyManagement下的依赖只是一个依赖申明，在子项目中还需要再次引入依赖，但是在子项目中不需要再次配置版本号，这样可以同一管理版本号。

    * Dependency:

      * Type 默认是jar

      * scop:定义使用场景

        * compile 编译:编译项目时使用（默认）。也会被打入包中。
        * test测试：测试阶段使用，不会被打包。spring-test在测试阶段有用在生产环境无用，因此不需要打包。如果不配置成test，造成打入无用包。
        * provided：编译时用于代码检查，打包时不打包。例子：servlet包。Tomcat有这些 包了，不需要再打包。
        * runtime：运行的时候采用，其他时候不需要。依赖接口，不关心具体实现类，在编译的时候不需要编译实现类。运行时才需要实现类，例子：JDBC。
        * system：本地的jar，还要定义systemPath——定义jar的目录。

      * 依赖传递

        * ![1532961555221](./assets/1532961555221.png)
        * 合理的使用依赖传递，可以消除依赖的冗余。
        * 子项目可以依赖父项目的依赖库，依赖具有传递性。

      * scop的传递性：

        * |          | compile  | test | provided | runtime  |
          | :------- | -------- | ---- | -------- | -------- |
          | compile  | compile  | -    | -        | runtime  |
          | test     | test     | -    | -        | test     |
          | provided | provided | -    | provided | provided |
          | runtime  | runtime  | -    | -        | runtime  |

          从上往下看，“-”表示不传递。

      * 一个库被多次依赖时，最短路径原则

        全局pom升级项目

        ```xml
        <build>
        	<plugins>
            	<plugin>
                	<groupId>org.codehaus.mojo</groupId>
                    <artifactId>versions-maven-plugin</artifactId>
                   	<version>2.3</version>
                </plugin>
            </plugins>
        </build>
        ```

        然后运行命令：

        ```
        mvn versions:set -DnewVersion=1.1-SNAPSHOT
        ```

        就完成的整体的升级。

        * 当一个库被多次依赖时，路径最短的那个生效。

      * 书写顺序原则：多次依赖，且路径长度相同时，以配置文件的先后顺序来定。

        * 是

      * exclusions:排除jar，解决冲突。重复以来的通过这个方式进行排除。

    * 生命周期lifecycle/phase/goal

      * clean
        * pre-clean
        * clean
        * post-clean
      * default
        * compile
        * package
        * install
        * deploy
        * ...
      * site
        * pre-site
        * site
        * post-site
        * site-deploy
      * A Build Lifecycle is Made Up of Phases 
      * A Build Phase is Made Up of Plugin Goals 

    * ![1533461795013](./assets/1533461795013.png)

    * ![1533461821774](./assets/1533461821774.png)

## 版本管理

1.   1.0-SNAPSHOT:当版本不稳定的时候对外提供一个API（SNAPSHOT是不稳定版本）

2. `SNAPSHOT版本是可以重复更新的，在上线的时候一定不能使用SNAPSHOT版本，会出问题的。RELEASE版本是不会被覆盖的，要发布，只能版本号加一否则推不上去。`

   1. 如果队友更新了jar包，要手动去更新，更新方法是：
      1.   repository 删除
      2.  mvn clean package -U (强制拉一次)

3.  主版本号.次版本号.增量版本号-<里程碑版本>

   1.0.0-RELAESE

## 常用命令

* compile 编译
* clean 删除 target及下的文件
* test  运行所有的test case junit/testNG
  * test->pro-scan顺序，模块配置顺序+pom依赖顺序
* package 打包
* install 把项目安装到本地仓库，如果你改了其他模块，并且这个模块被其他模块依赖，那在你运行insall之前是不会更新到依赖的那个项目的。
* deploy  将本地jar发布到remote。

## 插件

* 常用插件
  *  <https://maven.apache.org/plugins/> 
  *  http://www.mojohaus.org/plugins.html
  * findbugs 静态代码检查
  * versions 插件——统一升级版本
  * source 用于打source包，在install阶段——一般不对外使用
  * assembly 打包插件 zip 、war,可以打一个能单独运行的jar包（有main函数）
  * tomcat插件 

## 自定义插件

* 新建项目
* 改变pom打包类型为：maven-plugin    

```xml
 <packaging>maven-plugin</packaging>
```



* 配置项目依赖

```xml
 <dependencies>
        <dependency>
            <groupId>org.apache.maven</groupId>
            <artifactId>maven-plugin-api</artifactId>
            <version>3.5.0</version>
        </dependency>

        <dependency>
            <groupId>org.apache.maven.plugin-tools</groupId>
            <artifactId>maven-plugin-annotations</artifactId>
            <version>3.3</version>
            <scope>provided</scope>
        </dependency>

    </dependencies>
```

* 创建类，并继承`AbstractMojo`

```java

/**
 * Create by xdf on 2018/8/1
 *
 */
@Mojo(name="gupaoedu",defaultPhase = LifecyclePhase.PACKAGE)
public class GupaoMojo extends AbstractMojo {
    public void execute() throws MojoExecutionException, MojoFailureException {
        System.out.println("沽泡插件");
    }
}
```

解释用一下：

1. name：goal名——就是在使用的时候的goal名
2. ...

* 插件的使用

  * 将插件项目install进本地仓库
  * 在新建的项目中导入plugin
  * 配置插件执行的phase和goals

  ```xml
  <build>
          <plugins>
              <plugin>
                  <groupId>com.gupao</groupId>
                  <artifactId>gupao-plugin</artifactId>
                  <version>1.0-SNAPSHOT</version>
                  <executions>
                      <execution>
                          <phase>package</phase>
                          <goals>
                              <goal>gupaoedu</goal>
                          </goals>
                      </execution>
                  </executions>
              </plugin>
  
          </plugins>
  
      </build>
  ```

  配置好后，插件就能在运行到maven相应的phase的时候执行插件。

* maven传参

  * 在Mojo实现类中定义参数变量，并用 `@Parameter`标注

    ```java
    /**
     * Create by xdf on 2018/8/1
     *
     */
    @Mojo(name="gupaoedu",defaultPhase = LifecyclePhase.PACKAGE)
    public class GupaoMojo extends AbstractMojo {
        @Parameter
        private String msg;
        public void execute() throws MojoExecutionException, MojoFailureException {
            System.out.println("沽泡插件");
            System.out.println(msg);
        }
    }
    ```

  * 在使用插件项目的pom配置插件的configuration

    ```xml
    <build>
            <plugins>
                <plugin>
                    <groupId>com.gupao</groupId>
                    <artifactId>gupao-plugin</artifactId>
                    <version>1.0-SNAPSHOT</version>
                    <executions>
                        <execution>
                            <phase>package</phase>
                            <goals>
                                <goal>gupaoedu</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <msg>hello world</msg>
                    </configuration>
                </plugin>
            </plugins>
        </build>
    ```


  输出如下

  ```
  娌芥场鎻掍欢
  hello world
  ```


`maven有自己的内置参数，看用使用${}来引用`

## Profile

* 使用场景：分环境打包

* 如何使用
  * 在resource目录下定义不同的配置文件，打包的时候使用不同的配置文件就行了；

    * 配置profile

    ```xml
    <profiles>
            <profile>
                <id>dev</id>
                <properties>
                    <!-- 定义变量在后面配置使用的时候有用 -->
                    <profiles.active>dev</profiles.active>
                </properties>
                <activation>
                    <activeByDefault>true</activeByDefault>
                </activation>
            </profile>
    
            <profile>
                <id>test</id>
                <properties>
                    <profiles.active>test</profiles.active>
                </properties>
            </profile>
    
            <profile>
                <id>pro</id>
                <properties>
                    <profiles.active>pro</profiles.active>
                </properties>
            </profile>
        </profiles>
    ```
    * 分别创建dev/test/pro环境的配置文件

    ![1533457587897](./assets/1533457587897.png)

    * 再配置resource

    ```xml
    	<build>
    		<resources>
                <resource>
                    <directory>${basedir}/src/main/resources</directory>
                    <excludes>
                        <exclude>conf/**</exclude>
                    </excludes>
                </resource>
    
                <resource>
                    <!-- profiles.active  前面定义的变量-->
                    <directory>src/main/resources/conf/${profiles.active}</directory>
                </resource>
    
            </resources>
        </build>
    ```

  * 使用mvn命令编译

    * `mvn clean intall -P test`

  * 不同环境参数的时候，配置文件的内容是不一样的

    * ![1533457864932](./assets/1533457864932.png)

* profile在setting中也可以设置的

  * setting中的profile本机有效
  * 项目的profile本项目有效



## 仓库

搭建Maven私服

* 下载

* 安装 解压

* 使用<http://books.sonatype.com/nexus-book/reference3/index.html> 

  *  <http://192.168.1.6:8081/nexus>
  *   admin/admin123

* 发布

  *   pom.xml 配置 

  ![1533460793396](./assets/1533460793396.png)



  ![1533460807115](./assets/1533460807115.png)

* 下载jar配置

  * 配置mirror

  * Profile

## archetype  模版化

* 生成一个archetype
  *   mvn archetype:create-from-project
  *   cd /target/generated-sources/archetype
  * mvn install
  * 从archetype创建项目mvn archetype:generate -DarchetypeCatalog=local

