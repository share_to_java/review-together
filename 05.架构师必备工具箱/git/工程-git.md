# 工程化-git

## 什么是git

* git怎么来的——Linux core 有来自全世界的程序员贡献的代码，最初代码由githleper管理，后来收费了之后，Linux自己开发了git
* 开源协议——例如：部分开源框架要求如果使用了来它的开源框架，那么你开发的项目也必须是开源的。（一些开源框架： dubbo cat  ）
* 作用 Version Control



## git和SVN的区别

* SVN
  * SVN 是集中式的版本控制器（Centralized Version Controller System）
  * SVN架构图

  ![1533515135237](./assets/1533515135237.png)

  * SVN内部原理图

  ![1533515244591](./assets/1533515244591.png)

  * SVN的特点
    * 基于网络（需要连接server）
    * server的硬盘坏掉了恢复很困难

* Git

  * Git是分布式的代码控制器（Distribute Version Controller System）
  * git架构图
    * ![1533531226840](./assets/1533531226840.png)
    * 每个用户都是git的一个仓库
  * git原理图
    * ![1533531555072](./assets/1533531555072.png)
  * 如果断网了，那么git可以把本地作为仓库，进行代码版本控制
  * 如果git server出错了，那么只需要一个用户将代码push到服务器即可

* 总结

  * svn 是集中式的版本控制器，git是分布式的
  * svn在存储的时候存储的是文件变化的增量，git存储的是变化后的文件，如果没变化会存储一个文件，这个文件指向之前的版本

* git有SHA1文件摘要，用于保证文件的完整性——一些软件会把它的sha1发布出来，供用户验证，避免下载到非法版本。Linux使用sha256sum <xxx>.iso来计算文件的sha1摘要



## 安装

`sudo yum install  git`

### 配置

`git config --global  user.name "xxx"`

`git config --global user.email "xxx"`



SSH

`ssh-keygen -t rsa -C "your email address" `

`cat ~your profile address~/id_rsa.pub `

.....



## git多账户

* 在~/.ssh/下生成多套公私钥
* 创建config文件并编辑

```properties
Host git.oschian.com
HostName github.com
User git
IdentityFile C:\Users\XXX\.ssh\xxx(秘钥文件)

Host git.oschian.com
HostName git.oschian.com
User git
IdentityFile C:\Users\XXX1\.ssh\xxx1(秘钥文件)
```



## Git状态

![1533689367306](./assets/1533689367306.png)

![1533689385182](./assets/1533689385182.png)



## Git常用命令

* git statuts

* git remote

  * `git init`
  * 在远端创建新项目
  * `git remote add  < 项目地址>`
  * `git push -u origin master`

* git clone <xxxx>

* git init

* git push

  * git 回滚

  假设本地commit了3次，产生了ABC三个版本，并且都推送到了remote。现在发现C版本有bug，如果回滚到B?

  *  `git log`查看日志，找到要回滚的版本的commit id

  -  本地`git reset --hard <commit id>`
  - 推送到远端`git push -f origin master`

  ![1533690956543](./assets/1533690956543.png)

* git pull

* git checkout

  * 分支切换 `git checkout -b [branch]` `git checkout [branch]`
  * 撤销修改`git checkout .`
  * 在当前分支有未commit的修改时最好不要切换分支（可以用stash暂存任务，但容易忘记）

* git merge

  * 合并分支，最好将分支push到远端，用图形化界面合并

* rebase

* git tag（版本）

  * 创建一个tag用于管理版本(界面操作)

* git alias



## git flow

![1533773370044](./assets/1533773370044.png)

## gitlabs搭建

•<https://bitnami.com/stack/gitlab/virtual-machine> 

•<https://github.com/gitlabhq/gitlabhq> 

•[https://about.gitlab.com/downloads/#centos7](https://about.gitlab.com/downloads/) 